// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "U4_Behaviour.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class U4_API UU4_Behaviour : public UActorComponent
{
	GENERATED_BODY()

/// Player Controller Features
public: 
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_PLayerController, meta = (DisplayName = "@PlayerController"))
		APlayerController* PlayerController;

	UFUNCTION()
		void OnRep_PLayerController();

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "!SetController"))
		void fSetPlayerController(APlayerController* _player_controller);

public:	
	// Sets default values for this component's properties
	UU4_Behaviour();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION()
	void fControllerCheck(APlayerController* _pc, UU4_Controller_CAC* _cac);
};

