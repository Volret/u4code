// Fill out your copyright notice in the Description page of Project Settings.


#include "U4_Behaviour.h"

#include "Net/UnrealNetwork.h"

#include "U4_Controller_CAC.h"
#include "../Lib/U4_CoreLibrary.h"

void UU4_Behaviour::OnRep_PLayerController() 
{
	U4M_LOG_FUNC();
	PlayerController->FindComponentByClass<UU4_Controller_CAC>();
}

void UU4_Behaviour::fSetPlayerController(APlayerController* _player_controller) 
{
	U4M_LOG_FUNC();

	// Invoke RPC event.
	PlayerController = _player_controller;
	if(PlayerController) {
		APawn* owner = Cast<APawn>(GetOwner());
		if(owner && owner->HasAuthority()) {
			PlayerController->Possess(owner);
		}
	}
}

UU4_Behaviour::UU4_Behaviour()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UU4_Behaviour::BeginPlay()
{
	U4M_LOG_FUNC();
	Super::BeginPlay();	
}

// Called every frame
void UU4_Behaviour::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UU4_Behaviour::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const 
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UU4_Behaviour, PlayerController);
}

void UU4_Behaviour::fControllerCheck(APlayerController* _pc, UU4_Controller_CAC* _cac)
{
	U4M_LOG_FUNC();
}


