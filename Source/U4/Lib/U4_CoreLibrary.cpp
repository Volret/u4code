// Fill out your copyright notice in the Description page of Project Settings.


#include "U4_CoreLibrary.h"
#include "Misc\MessageDialog.h"

#include "U4/Public/U4_GI_CPP.h"
#include "U4/Public/U4_GM_CAC.h"

#include "GameFramework/Actor.h"
#include "GameFramework/GameMode.h"

#include "U4.h"

#include "Core.h"

UU4_GI_CPP* UU4_CoreLibrary::fGetGI(const UObject* _WorldContext)
{
    if (UWorld* world = GEngine->GetWorldFromContextObject(_WorldContext, EGetWorldErrorMode::LogAndReturnNull))
    {
        UU4_GI_CPP* result = Cast<UU4_GI_CPP> (world->GetGameInstance());
        if (result)
            return result;
    }
    fRiseError("U4 Internal Problem", "Can not found U4 Game Instance");
    return nullptr;
}


UU4_GM_CAC* UU4_CoreLibrary::fGetES(const UObject* _WorldContext) {

    if ( UWorld* world = GEngine->GetWorldFromContextObject(_WorldContext, EGetWorldErrorMode::LogAndReturnNull) ) {
        AGameModeBase* gm = world->GetAuthGameMode();
        if(gm) {
            if(UU4_GM_CAC* result = gm->FindComponentByClass<UU4_GM_CAC>())
                return result;
        }
    }
    return nullptr;
}

void UU4_CoreLibrary::fCreateStaticMesh() {
	/*
	// Object Details
		FString ObjectName = FString("MyObject");

		TArray Vertices;
		Vertices.Add(FVector(86.6, 75, 0));
		Vertices.Add(FVector(-86.6, 75, 0));
		Vertices.Add(FVector(2.13, 25, 175));
		Vertices.Add(FVector(2.13, -75, 0));
		int numberOfVertices = Vertices.Num();

		struct Face
		{
			unsigned int v1;
			unsigned int v2;
			unsigned int v3;
			short materialID;
			FVector2D uvCoords1;
			FVector2D uvCoords2;
			FVector2D uvCoords3;
		};
		TArray Faces;
		Face oneFace;
		oneFace = {1, 3, 0, 0, FVector2D(0, 0), FVector2D(1, 0), FVector2D(0.5, 1)};
		Faces.Add(oneFace);
		oneFace = {0, 2, 1, 1, FVector2D(0, 0), FVector2D(1, 0), FVector2D(0.5, 1)};
		Faces.Add(oneFace);
		oneFace = {3, 2, 0, 0, FVector2D(0, 0), FVector2D(1, 0), FVector2D(0.5, 1)};
		Faces.Add(oneFace);
		oneFace = {1, 2, 3, 1, FVector2D(0, 0), FVector2D(1, 0), FVector2D(0.5, 1)};
		Faces.Add(oneFace);
		int numberOfFaces = Faces.Num();

		TArray Materials; //This should contain the real Materials, this is just an example
		Materials.Add(FStaticMaterial());
		Materials.Add(FStaticMaterial());
		int numberOfMaterials = Materials.Num();

		// Create Package
		FString pathPackage = FString("/Game/MyStaticMeshes/");
		FString absolutePathPackage = FPaths::GameContentDir() + "/MyStaticMeshes/";

		FPackageName::RegisterMountPoint(*pathPackage, *absolutePathPackage);

		UPackage* Package = CreatePackage(nullptr, *pathPackage);

		// Create Static Mesh
		FName StaticMeshName = MakeUniqueObjectName(Package, UStaticMesh::StaticClass(), FName(*ObjectName));
		UStaticMesh* myStaticMesh = NewObject(Package, StaticMeshName, RF_Public | RF_Standalone);

		if(myStaticMesh != NULL) {
			FRawMesh myRawMesh = FRawMesh();
			FColor WhiteVertex = FColor(255, 255, 255, 255);
			FVector EmptyVector = FVector(0, 0, 0);

			// Vertices
			for(int vertIndex = 0; vertIndex < numberOfVertices; vertIndex++) {
				myRawMesh.VertexPositions.Add(Vertices[vertIndex]);
			}
			// Faces and UV/Normals
			for(int faceIndex = 0; faceIndex < numberOfFaces; faceIndex++) {
				myRawMesh.WedgeIndices.Add(Faces[faceIndex].v1);
				myRawMesh.WedgeIndices.Add(Faces[faceIndex].v2);
				myRawMesh.WedgeIndices.Add(Faces[faceIndex].v3);

				myRawMesh.WedgeColors.Add(WhiteVertex);
				myRawMesh.WedgeColors.Add(WhiteVertex);
				myRawMesh.WedgeColors.Add(WhiteVertex);

				myRawMesh.WedgeTangentX.Add(EmptyVector);
				myRawMesh.WedgeTangentX.Add(EmptyVector);
				myRawMesh.WedgeTangentX.Add(EmptyVector);

				myRawMesh.WedgeTangentY.Add(EmptyVector);
				myRawMesh.WedgeTangentY.Add(EmptyVector);
				myRawMesh.WedgeTangentY.Add(EmptyVector);

				myRawMesh.WedgeTangentZ.Add(EmptyVector);
				myRawMesh.WedgeTangentZ.Add(EmptyVector);
				myRawMesh.WedgeTangentZ.Add(EmptyVector);

				// Materials
				myRawMesh.FaceMaterialIndices.Add(Faces[faceIndex].materialID);

				myRawMesh.FaceSmoothingMasks.Add(0xFFFFFFFF); // Phong

				for(int UVIndex = 0; UVIndex < MAX_MESH_TEXTURE_COORDS; UVIndex++) {
					myRawMesh.WedgeTexCoords[UVIndex].Add(Faces[faceIndex].uvCoords1);
					myRawMesh.WedgeTexCoords[UVIndex].Add(Faces[faceIndex].uvCoords2);
					myRawMesh.WedgeTexCoords[UVIndex].Add(Faces[faceIndex].uvCoords3);
				}
			}

			// Saving mesh in the StaticMesh
			new(myStaticMesh->SourceModels) FStaticMeshSourceModel();
			myStaticMesh->SourceModels[0].RawMeshBulkData->SaveRawMesh(myRawMesh);

			FStaticMeshSourceModel& SrcModel = myStaticMesh->SourceModels[0];

			// Model Configuration
			myStaticMesh->SourceModels[0].BuildSettings.bRecomputeNormals = true;
			myStaticMesh->SourceModels[0].BuildSettings.bRecomputeTangents = true;
			myStaticMesh->SourceModels[0].BuildSettings.bUseMikkTSpace = false;
			myStaticMesh->SourceModels[0].BuildSettings.bGenerateLightmapUVs = true;
			myStaticMesh->SourceModels[0].BuildSettings.bBuildAdjacencyBuffer = false;
			myStaticMesh->SourceModels[0].BuildSettings.bBuildReversedIndexBuffer = false;
			myStaticMesh->SourceModels[0].BuildSettings.bUseFullPrecisionUVs = false;
			myStaticMesh->SourceModels[0].BuildSettings.bUseHighPrecisionTangentBasis = false;

			// Assign the Materials to the Slots (optional

			for(int32 MaterialID = 0; MaterialID < numberOfMaterials; MaterialID++) {
				myStaticMesh->StaticMaterials.Add(Materials[MaterialID]);
				myStaticMesh->SectionInfoMap.Set(0, MaterialID, FMeshSectionInfo(MaterialID));
			}

			// Processing the StaticMesh and Marking it as not saved
			myStaticMesh->ImportVersion = EImportStaticMeshVersion::LastVersion;
			myStaticMesh->CreateBodySetup();
			myStaticMesh->SetLightingGuid();
			myStaticMesh->PostEditChange();
			Package->MarkPackageDirty();

			UE_LOG(LogTemp, Log, TEXT("Static Mesh created: %s"), &ObjectName);

		}
		*/
}

void UU4_CoreLibrary::fRiseError(FString _Title, FString _Text)
{
    FMessageDialog::Open(EAppMsgType::Ok, FText::FromString(FString("Error: ") + _Title + FString(" - ") + _Text));
}

void UU4_CoreLibrary::fLog(FString _Title, FString _Text)
{
    UE_LOG(U4, Log, TEXT("%s : %s"), *_Title, *_Text);
}

void UU4_CoreLibrary::fError(FString _Title, FString _Text)
{
	UE_LOG(U4, Error, TEXT("Error: %s : %s"), *_Title, *_Text);
}
