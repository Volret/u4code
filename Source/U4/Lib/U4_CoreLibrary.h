// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Kismet/BlueprintFunctionLibrary.h"
#include "U4_CoreLibrary.generated.h"

class UU4_GI_CPP;
class UU4_GM_CAC;

// Macro NOTHING do nothing, just for simply read
#define NOTHING()

// U4M_LOG_FUNC() print to log name of its function
#if defined U4_NOLOG
	#define U4M_LOG_FUNC() NOTHING()
#else
	#define U4M_LOG_FUNC() UU4_CoreLibrary::fLog("Execution",__FUNCTION__);
#endif

// U4M_LOG_STEP print to log whats step trying to start
#ifndef U4_NOLOG
#define U4M_LOG_STEP(_text) UU4_CoreLibrary::fLog(_text,"in "##__FUNCTION__);
#else
#define U4M_LOG_STEP() NOTHING()
#endif

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FClassicDelegate);

UCLASS()
class U4_API UU4_CoreLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(Category = "U4", BlueprintCallable, meta = (WorldConext="_WorldContext", DisplayName = ".GetGameInstance"))
		static UU4_GI_CPP* fGetGI(const UObject* _WorldContext);

	UFUNCTION(Category = "U4", BlueprintCallable, meta = ( WorldConext = "_WorldContext", DisplayName = ".GetEventSystem" ))
		static UU4_GM_CAC* fGetES(const UObject* _WorldContext);

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "FAQ.Class"))
		static void fShowClass(TSubclassOf<AActor> _Class) {}

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "FAQ.Level"))
		static void fShowLevel(TSoftObjectPtr<UWorld> _Level ) {}

	static void fCreateStaticMesh();

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = ".RiseError(With Msg Dialog)"))
		static void fRiseError(FString _Title, FString _Text);

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = ".Log(Log only)"))
		static void fLog(FString _Title, FString _Text);

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = ".Log(Log only)"))
		static void fError(FString _Title, FString _Text);
};