// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../Lib/U4_CoreLibrary.h"

#include "U4_Event_CAC.generated.h"


UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class U4_API UU4_Event_CAC : public UActorComponent
{
	GENERATED_BODY()

public:// Event State (Replicated)
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_EventStateChanged, meta=(DisplayName = "@EventState"))
	int32 EventState;

	UFUNCTION()
	void OnRep_EventStateChanged();

	UFUNCTION(BlueprintImplementableEvent)
	void EventStateChanged(int32 _event_state);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "!SetState"))
	void fEventStateSet(int32 _value) {
		fSetEventState(_value);
	}

	FORCEINLINE 
	void fSetEventState(int32 _value) {
		EventState = _value;
		if (GetOwner()->HasAuthority()) {
			cbEventStateServer.Broadcast();
		}
	}

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "!Run"))
	void fStart() {
		fSetEventState(1);
		cbEventStart.Broadcast();
	}

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "!Stop"))
	void fStop() {
		fSetEventState(0);
		cbEventStop.Broadcast();
	}

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "!Stop"))
	void fDone() {
		fSetEventState(0);
		cbEventDone.Broadcast();
	}

public: // Event Features.
	UPROPERTY(BlueprintReadWrite, meta = ( DisplayName="@Features" ))
	TArray<FString> Features;

public:// Dispatcers for blueprint binding messages.
	UPROPERTY(BlueprintAssignable)
	FClassicDelegate cbEventStateClient;

	UPROPERTY(BlueprintAssignable)
	FClassicDelegate cbEventStateServer;

	UPROPERTY(BlueprintAssignable)
	FClassicDelegate cbEventDone;

	UPROPERTY(BlueprintAssignable)
	FClassicDelegate cbEventStart;

	UPROPERTY(BlueprintAssignable)
	FClassicDelegate cbEventStop;

public:	
	UU4_Event_CAC();

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};
