// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Player/U4_Controller_Interface.h"


#include "U4_Controller_CAC.generated.h"


class UU4_Event_CAC;
class UU4_Behaviour;

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class U4_API UU4_Controller_CAC : public UActorComponent {// : public IU4_Controller_Interface {
	GENERATED_BODY()

public:
	struct Controller {
		APlayerController* PlayerController;
		bool IsLocallyControlled;
	} Controller;

	UFUNCTION(meta=(DisplayName="!ControllerPoccess"))
		void fControllerPoccess(APawn* _pawn);

	UFUNCTION(meta = (DisplayName = "!OnControllerPoccess"), BlueprintImplementableEvent)
		void ControllerPoccessBP();

// Game Master Section
public:
	
	UPROPERTY(BlueprintReadOnly, meta=(DisplayName="@IsGameMaster"))
	bool IsGameMaster;
	FORCEINLINE void fSetMaster(bool _value) {
		UWorld* world = GEngine->GetWorldFromContextObject(GetOwner(), EGetWorldErrorMode::LogAndReturnNull);
		IsGameMaster = _value;
		if ( _value ) {
			GameMasterWorld();
		}
	}
	
	class InputInterfaceKeeper {
		TArray<IU4_Controller_Interface*> InputStack;

	public:
		void fAdd(IU4_Controller_Interface* _value) {
			InputStack.Add(_value);
		}
		void fRemove(IU4_Controller_Interface* _value) {
			InputStack.Remove(_value);
		}
	} InputKeeper;

	FTimerHandle GameMasterDelay;

// Behaviour Section
public:
	UPROPERTY()
		UU4_Behaviour* Behaviour;

	UFUNCTION()
		void fNewBehaviour(UU4_Behaviour* _new_behaviour);

public:	
	// Sets default values for this component's properties
	UU4_Controller_CAC();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	// Called with NewPawnDelegate from Controller
	
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
		
	// When world brings authority controller, the worlds became U4 World.
	// ����� ��������� ���������� � ��������, ���� ���������� ������������ ��� ��� U4, ��� ��������� ��� ���������� � ��� ������������� � ����������� ����.
	virtual void fInitiateAuthorityWorld(const UWorld* _world);
	virtual void fInitiateRemoteWorld(const UWorld* _world);
	virtual void fInitiateGameMasterWorld(const UWorld* _world);

	// The best way to change Event State on server
	UFUNCTION(BlueprintCallable, meta = ( DisplayName = "!SetEventState(on Client)" ))
		void fSetEventState(UU4_Event_CAC* _event, int _state, bool _server);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "!AddInputBehaviour"))
		void fAddInputBehaviour(UObject* _behaviour);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "!RemoveInputBehaviour"))
		void fRemoveInputBehaviour(UObject* _behaviour);

	// Run U4_Event on server with param
	UFUNCTION(Server, Reliable, meta = ( DisplayName="!SetEventState(to Server)" ))
		void fSetEventStateOnServer(UU4_Event_CAC* _event, int _state);

public: // Worlds initialization
	UFUNCTION(BlueprintImplementableEvent)
		void AuthorityWorld();

	UFUNCTION(BlueprintImplementableEvent)
		void RemoteWorld();

	UFUNCTION(BlueprintImplementableEvent)
		void GameMasterWorld();
};

