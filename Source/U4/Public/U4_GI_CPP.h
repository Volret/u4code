// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include <set>

#include "U4_GI_CPP.generated.h"

class UU4_Controller_CAC;

UCLASS(Blueprintable)
class U4_API UU4_GI_CPP : public UGameInstance
{
	GENERATED_BODY()
private:
	// Level Keeper class.
	class LevelKeeper {
		FString Level;
		// Struct of the level.
		struct LevelStruct {
			int32 Episode;
			int32 Act;
			FString Level;

			LevelStruct(int32 _E, int32 _A, FString _L) : Episode(_E), Act(_A), Level(_L) {}
			LevelStruct(int32 _E, int32 _A) : Episode(_E), Act(_A) {}

			friend bool operator < (const LevelStruct& _L, const LevelStruct& _R) {
				if (_L.Episode > _R.Episode) return true;
				if (_L.Act > _R.Act) return true;
				return false;
			}

			friend bool operator == (const LevelStruct & _L, const LevelStruct & _R) {
				if (_L.Episode == _R.Episode && _L.Act == _R.Act) return true;
				return false;
			}
		};
		typedef std::set<LevelStruct> T_Levels;
		T_Levels Levels;

	public:
		void fAddLevel(int _E, int _A, FString _Level) {
			Levels.insert(LevelStruct(_E, _A, _Level));
		}
		bool fGetLevel(FString& _Level, int _E, int _A) {
			const T_Levels::iterator result = Levels.find(LevelStruct(_E, _A));
			if (result != Levels.end()) {
				_Level = (*result).Level;
				return true;
			}
			return false;
		}
	} LevelKeeper;

	// Networking Keeper class.
	class NetworkingKeeper {
		int32 IsNetwokrkAvailable:1;
		int32 AllreadyInSession:1;
	public:
		NetworkingKeeper() : IsNetwokrkAvailable(1), AllreadyInSession(1) {}
	} NetKeeper;

	struct Initialize	{
		int32 bInited:1;
		Initialize() : bInited(1) {}

		// This func can be called only once on level. If not == critical error.
		void Check() {
			check(bInited);
			bInited = 0x0;
		}

		void Reset() {
			bInited = 0x1;
		}
	}InitializeChecker;

public:
	// Calls from controller once on each device.
	void fControllerCheck(APlayerController* _controller, UU4_Controller_CAC* _cac);

	UFUNCTION(BlueprintCallable, meta = (WorldContext = "_WorldContext", AdvancedDisplay = "3", DisplayName = "Code.Test(SelfTest)"), Category = "Game")
		void fSelfTest(const UObject* _WorldContext);

	UFUNCTION(BlueprintCallable, meta = (WorldContext = "_WorldContext", AdvancedDisplay = "3", DisplayName = "Code.Travel(by Episode)"), Category = "Game")
		void fTravel(const UObject* _WorldContext, int _E, int _A);

	UFUNCTION(BlueprintCallable, DisplayName = "Code.RegisterMap(by Name)", Category = "Game")
		void fAddMap(FString _Map, int _Episode, int _Act);

	UFUNCTION(BlueprintCallable, DisplayName = "Code.RegisterMap(by World)", Category = "Game")
		void fAddWorld(const TSoftObjectPtr<UWorld> _Level, int _Episode, int _Act);

};

