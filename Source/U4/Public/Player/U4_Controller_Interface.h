// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "U4_Controller_Interface.generated.h"

UINTERFACE(Blueprintable, MinimalAPI, meta = (DisplayName = "U4 Input Interface"), Category = "U4")
class UU4_Controller_Interface : public UInterface {
	GENERATED_BODY()
	
};

class IU4_Controller_Interface {
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool L_StickX(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool L_StickY(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool R_StickX(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool R_StickY(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool Right(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool Down(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool Left(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool Up(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool R_Down(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool L_Down(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool Y(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool X(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool B(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool A(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool Select(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool Start(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool RB(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool RT(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool LB(float _value);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		bool LT(float _value);
};

