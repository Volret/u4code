// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "U4_GS_CAC.generated.h"

class UU4_Controller_CAC;

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class U4_API UU4_GS_CAC : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UU4_GS_CAC();

public: // Current map behaviour.
	UPROPERTY(BlueprintReadOnly, meta = (DisplayName = "@CurrentMap"))
		UU4_Map_CAC* CurrentMap;

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!RegisterMap"))
		bool fRegisterMap(UU4_Map_CAC* _map);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void fControllerCheck(APlayerController* _controller, UU4_Controller_CAC* _cac);

		
};

