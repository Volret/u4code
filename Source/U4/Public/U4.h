// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Modules/ModuleManager.h"


DECLARE_LOG_CATEGORY_EXTERN(U4, Log, All);

class FU4Module : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	void fModuleInititalise();
	FORCEINLINE static void fMessage();

private:
	/** Handle to the test dll we will load */
	void*	ExampleLibraryHandle;
};

