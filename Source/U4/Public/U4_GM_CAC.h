// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "U4_Controller_CAC.h"

#include "../Public/U4_Event_CAC.h"

#include "U4_GM_CAC.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNewMasterDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNewPlayerDelegate);

class UU4_Map_CAC;

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class U4_API UU4_GM_CAC : public UActorComponent
{
	GENERATED_BODY()

public: // ! Players.
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (DisplayName = "@Players"))
	TArray<APlayerController*> Players;

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!SetWorldMaster"))
	void fSetWorldMaster(APlayerController* _PC);

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!AddPlayer"))
	void fAddPlayer(APlayerController* _PC);

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!RemovePlayer"))
	void fRemovePlayer(APlayerController* _PC);

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!FindPlayer"))
	APlayerController* fGetPlayer(FString _player_name);

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!KickPlayer"))
	APlayerController* fKickPlayer(FString _player_name, FText _reason);

	// @ Game Master - first player controller
	UPROPERTY(BlueprintReadWrite, meta = (DisplayName = "@GameMaster"))
	const APlayerController* GameMaster;

	// @ Actual Game Master - the master who allready takes leader rights
	UPROPERTY(BlueprintReadWrite, meta = (DisplayName = "@ActualGameMaster"))
	UU4_Controller_CAC* ActualMaster;


public: // ! Events.
	struct Events {
		TMultiMap<FString, UU4_Event_CAC*>		RegisteredEvents;
		TSet<FString>							RunningEvents;
		TSet<FString>							DoneEvents;

		void fRegisterEvent(UU4_Event_CAC* _e) {
			check(_e);
			for (FName name : _e->ComponentTags) {
				FString tag = name.ToString();
				if (tag.Left(1) == TEXT("!")) {
					RegisteredEvents.Add(tag, _e);
					if (RunningEvents.FindId(tag).IsValidId()) {
						_e->fStart();
					} 
					else if (DoneEvents.FindId(tag).IsValidId()) {
						_e->fDone();
					}
				}
			}
		}

		void fRemoveEvent(UU4_Event_CAC* _e) {
			check(_e);
			for (FName name : _e->ComponentTags) {
				FString tag = name.ToString();
				if (tag.Left(1) == "!") {
					RegisteredEvents.Remove(tag);
				}
			}
		}

		void fPrintEvents() {
			for (TPair<FString, UU4_Event_CAC*>& el : RegisteredEvents) {
				UU4_CoreLibrary::fLog(el.Key, el.Value->GetOwner()->GetActorLabel());
			}
			UU4_CoreLibrary::fLog(TEXT("-------"), TEXT("-------"));
		}

		void fStartEvent(FString _event) {
			if (!RunningEvents.FindId(_event).IsValidId()) {
				RunningEvents.Add(_event);
				TArray<UU4_Event_CAC*> result;
				RegisteredEvents.MultiFind(_event, result);
				for (UU4_Event_CAC* e : result) {
					check(e);
					e->fStart();
				}
			}
		}

		void fStopEvent(FString _event) {
			if (RunningEvents.FindId(_event).IsValidId()) {
				TArray<UU4_Event_CAC*> result;
				RegisteredEvents.MultiFind(_event, result);
				for (UU4_Event_CAC* e : result) {
					check(e);
					e->fStop();
				}
				RunningEvents.Remove(_event);
			}
		}

		void fDoneEvent(FString _event) {
			if (RunningEvents.FindId(_event).IsValidId()) {
				TArray<UU4_Event_CAC*> result;
				RegisteredEvents.MultiFind(_event, result);
				for (UU4_Event_CAC* e : result) {
					check(e);
					e->fStop();
				}
				RunningEvents.Remove(_event);
			}
		}

	}Events;
		

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!RegisterEvent"))
	void fRegisterEvent(UU4_Event_CAC* _e) {
		Events.fRegisterEvent(_e);
	}

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!UnregisterEvent"))
	void fRemoveEvent(UU4_Event_CAC* _e) {
		Events.fRemoveEvent(_e);
	}

	UPROPERTY(BlueprintReadOnly, meta = (DisplayName = "@IsGameStart"))
	bool IsGameStart;

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!GameStart"))
	void fGameStart();

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!RunEvent"))
	void fRunEvent(FString _event) {
		Events.fStartEvent(_event);
	}

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!StopEvent"))
	void fStopEvent(FString _event) {
		Events.fStopEvent(_event);
	}

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!IsEventStarted"))
	bool fIsEventStarted(FString _tag) {
		return false;
	}

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!IsEventDone"))
	bool fIsEventDone(FString _tag) {
		return false;
	}

public: // Current map behaviour.
	UPROPERTY(BlueprintReadOnly, meta = (DisplayName = "@CurrentMap"))
	UU4_Map_CAC* CurrentMap;

	UFUNCTION(Category = "U4", BlueprintCallable, meta = (DisplayName = "!RegisterMap"))
	bool fRegisterMap(UU4_Map_CAC* _map);

public: // ! Delegates.
	UPROPERTY(BlueprintAssignable)
	FNewMasterDelegate cbNewMaser;

	UPROPERTY(BlueprintAssignable)
	FClassicDelegate cbGameStart;

	UPROPERTY(BlueprintAssignable)
	FNewPlayerDelegate cbNewPlayerController;

	UPROPERTY(BlueprintAssignable)
	FClassicDelegate cbNewMap;

public:	
	// Sets default values for this component's properties
	UU4_GM_CAC();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void fControllerCheck(APlayerController* _controller, UU4_Controller_CAC* _cac);

private: // Service
	UU4_Event_CAC* fGetEventWithTag(FName Tag) const;
};
