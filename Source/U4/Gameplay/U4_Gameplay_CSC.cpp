// Fill out your copyright notice in the Description page of Project Settings.


#include "U4_Gameplay_CSC.h"

// Sets default values for this component's properties
UU4_Gameplay_CSC::UU4_Gameplay_CSC()
{
	PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void UU4_Gameplay_CSC::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UU4_Gameplay_CSC::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}


