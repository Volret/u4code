// Fill out your copyright notice in the Description page of Project Settings.


#include "U4_Event_CAC.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UU4_Event_CAC::UU4_Event_CAC()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}



// Called when the game starts
void UU4_Event_CAC::BeginPlay()
{
	Super::BeginPlay();

	AGameModeBase* gm = GetWorld()->GetAuthGameMode();
	if (gm) {
		UU4_GM_CAC* gm_cac = gm->FindComponentByClass<UU4_GM_CAC>();
		if (gm_cac) {
			gm_cac->Events.fRegisterEvent(this);
			gm_cac->Events.fPrintEvents();
		}
	}
}

void UU4_Event_CAC::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	AGameModeBase* gm = GetWorld()->GetAuthGameMode();
	if (gm) {
		UU4_GM_CAC* gm_cac = gm->FindComponentByClass<UU4_GM_CAC>();
		if (gm_cac) {
			gm_cac->fRemoveEvent(this);
		}
	}
}


// Called every frame
void UU4_Event_CAC::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UU4_Event_CAC::OnRep_EventStateChanged( ) 
{
	U4M_LOG_FUNC();
	EventStateChanged(EventState);
	cbEventStateClient.Broadcast();
}

void UU4_Event_CAC::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const 
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UU4_Event_CAC, EventState);
}

