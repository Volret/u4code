// Fill out your copyright notice in the Description page of Project Settings.


#include "U4_GS_CAC.h"
#include "../Lib/U4_CoreLibrary.h"

// Sets default values for this component's properties
UU4_GS_CAC::UU4_GS_CAC()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

// Called when the game starts
void UU4_GS_CAC::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void UU4_GS_CAC::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UU4_GS_CAC::fControllerCheck(APlayerController* _controller, UU4_Controller_CAC* _cac) {
	U4M_LOG_FUNC();
}

bool
UU4_GS_CAC::fRegisterMap(UU4_Map_CAC* _map)
{
	if (CurrentMap != nullptr) {
		CurrentMap = _map;
		return true;
	}
	else {
		return false;
	}
}

