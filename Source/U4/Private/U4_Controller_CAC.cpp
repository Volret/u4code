// Fill out your copyright notice in the Description page of Project Settings.


#include "U4_Controller_CAC.h"
#include "U4_GM_CAC.h"
#include "U4_GS_CAC.h"
#include "U4_Event_CAC.h"
#include "U4.h"

#include "../Lib/U4_CoreLibrary.h"
#include "Player/U4_Controller_Interface.h"

#include "GameFramework/Actor.h"
#include "GameFramework/GameState.h"

void UU4_Controller_CAC::fNewBehaviour(UU4_Behaviour* _new_behaviour) 
{
	U4M_LOG_FUNC();
	Behaviour = _new_behaviour;
	if(Behaviour) {
		Behaviour->fControllerCheck(Controller.PlayerController, this);
	}
}

// Sets default values for this component's properties
UU4_Controller_CAC::UU4_Controller_CAC()
{
	PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void UU4_Controller_CAC::BeginPlay()
{
	U4M_LOG_FUNC( );
	Super::BeginPlay();

	UWorld* world = GEngine->GetWorldFromContextObject(GetOwner(), EGetWorldErrorMode::LogAndReturnNull);
	Controller.PlayerController = Cast<APlayerController>(GetOwner());
	check(Controller.PlayerController);
	Controller.IsLocallyControlled = Controller.PlayerController->IsLocalController();

	if (Controller.PlayerController->HasAuthority())
	{
		fInitiateAuthorityWorld(world);
	}
	else
	{
		fInitiateRemoteWorld(world);
	}
}

void UU4_Controller_CAC::fControllerPoccess(APawn* _pawn) 
{
	U4M_LOG_FUNC();
	ControllerPoccessBP();
}

void UU4_Controller_CAC::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UU4_Controller_CAC::fInitiateAuthorityWorld(const UWorld* _world)
{
	U4M_LOG_FUNC();
	AGameModeBase* game_mode = _world->GetAuthGameMode();

	check(game_mode);
	UU4_GM_CAC* cac = game_mode->FindComponentByClass<UU4_GM_CAC>();
	if (cac) {
		cac->fAddPlayer(Controller.PlayerController);
	}
	else if(true){// Fast call to create game master world
		fInitiateGameMasterWorld(_world);
	}
	else { // Delayed call to create U4 world.
		Controller.PlayerController->GetWorldTimerManager().SetTimer(GameMasterDelay, [this, _world] () {
			fInitiateGameMasterWorld(_world);
		}, 10.2f, 0);
	}

	// Bind Posses.
	FPawnChangedSignature& new_pawn_delegate = Controller.PlayerController->GetOnNewPawnNotifier();
	new_pawn_delegate.AddUFunction(this, TEXT("fControllerPoccess"));

	AuthorityWorld();
}

void UU4_Controller_CAC::fInitiateRemoteWorld(const UWorld* _world)
{
	U4M_LOG_FUNC( );
	UU4_CoreLibrary::fLog("Controller initialize Remote", Controller.PlayerController->GetActorLabel( ));

	RemoteWorld();
}

void UU4_Controller_CAC::fInitiateGameMasterWorld(const UWorld* _world)
{
	U4M_LOG_FUNC();

	// Initialize Game Mode
	AGameModeBase* game_mode = _world->GetAuthGameMode();
	check(game_mode);
	const UU4_GM_CAC* gm_cac = game_mode->FindComponentByClass<UU4_GM_CAC>();
	if(!gm_cac) {
		UU4_GM_CAC* new_component = NewObject<UU4_GM_CAC>(game_mode, UU4_GM_CAC::StaticClass(), TEXT("U4_EventSystem"), RF_DefaultSubObject);
		check(new_component);
		new_component->RegisterComponent();
		new_component->fControllerCheck(Controller.PlayerController, this);
		new_component->fAddPlayer(Controller.PlayerController);
	}

	// Initialize Game Istance
	UU4_GI_CPP* game_inst = UU4_CoreLibrary::fGetGI(_world);
	if(game_inst) {
		game_inst->fControllerCheck(Controller.PlayerController, this);
	}


	// Initialize Game State
	AGameStateBase* game_state = _world->GetGameState();
	if (game_state) {
		UU4_GS_CAC* game_state_cac = game_state->FindComponentByClass<UU4_GS_CAC>();
		check(game_state);
		if (!game_state_cac) {
			// Add Game State CAC to game state.
			UU4_GS_CAC* new_gs_cac = NewObject<UU4_GS_CAC>(game_state, UU4_GS_CAC::StaticClass(), TEXT("U4_EventSystem"), RF_DefaultSubObject);
			new_gs_cac->RegisterComponent();
			new_gs_cac->fControllerCheck(Controller.PlayerController, this);
		}
	}
	else {
		// Game state still not created
		UU4_CoreLibrary::fError("Game State not found yet", GetOwner()->GetActorLabel());
	}

	UU4_CoreLibrary::fLog("Controller initialize Game Master", GetOwner()->GetActorLabel( ));

	GameMasterWorld();
}

void UU4_Controller_CAC::fSetEventState(UU4_Event_CAC* _event, int _state, bool _server) 
{
	U4M_LOG_FUNC( );
	check(_event);
	if ( _server ) {
		fSetEventStateOnServer( _event, _state);
		return;
	}
	_event->fSetEventState(_state);
}

void UU4_Controller_CAC::fAddInputBehaviour(UObject* _behaviour)
{
	check(_behaviour);
	IU4_Controller_Interface* interface_behaviour = Cast<IU4_Controller_Interface>(_behaviour);
	check(interface_behaviour);
	InputKeeper.fAdd(interface_behaviour);
}

void UU4_Controller_CAC::fRemoveInputBehaviour(UObject* _behaviour)
{
	check(_behaviour);
	IU4_Controller_Interface* interface_behaviour = Cast<IU4_Controller_Interface>(_behaviour);
	check(interface_behaviour);
	InputKeeper.fRemove(interface_behaviour);
}

void UU4_Controller_CAC::fSetEventStateOnServer_Implementation(UU4_Event_CAC* _event, int _state) 
{
	U4M_LOG_FUNC( );
	check(_event);
	check(Controller.PlayerController);
	check(Controller.PlayerController->HasAuthority());
	_event->fSetEventState(_state);
}

