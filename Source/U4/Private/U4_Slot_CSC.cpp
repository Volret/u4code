// Fill out your copyright notice in the Description page of Project Settings.


#include "U4_Slot_CSC.h"

// Sets default values for this component's properties
UU4_Slot_CSC::UU4_Slot_CSC()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UU4_Slot_CSC::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UU4_Slot_CSC::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}
