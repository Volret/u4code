// Fill out your copyright notice in the Description page of Project Settings.


#include "U4_GM_CAC.h"

#include "U4_Event_CAC.h"

#include "GameFramework/GameSession.h"
#include "GameFramework/GameMode.h"
#include "EngineUtils.h"

#include "../Lib/U4_CoreLibrary.h"

// Sets default values for this component's properties
UU4_GM_CAC::UU4_GM_CAC()
{
	PrimaryComponentTick.bCanEverTick = false;
	IsGameStart = nullptr;
	CurrentMap = nullptr;
}


// Called when the game starts
void UU4_GM_CAC::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UU4_GM_CAC::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UU4_GM_CAC::fControllerCheck(APlayerController* _controller, UU4_Controller_CAC* _cac)
{
	U4M_LOG_FUNC();
	check(_controller);
	check(_cac);
	fAddPlayer(_controller);
}

void UU4_GM_CAC::fSetWorldMaster(APlayerController* _PC)
{
	U4M_LOG_FUNC();
	UU4_Controller_CAC* controller_component = nullptr;
	if (_PC) {
		controller_component = _PC->FindComponentByClass<UU4_Controller_CAC>();
		if (!controller_component) {
			UU4_CoreLibrary::fRiseError(FString("World Master controller must have CAC component"), FString("UU4_GM_CAC error"));
			return;
		}
		if (ActualMaster) {
			if (ActualMaster != controller_component) {
				return;
			} else {
				ActualMaster->fSetMaster(false);
			}
		}
		ActualMaster = controller_component;
		ActualMaster->fSetMaster(true);
	}
	else {
		if (ActualMaster)
			ActualMaster->fSetMaster(false);
	}
	GameMaster = _PC;
	// Runnning new master event if exists.
	fRunEvent("!NewMasterEvent()");
	cbNewMaser.Broadcast();
	return;
}

void UU4_GM_CAC::fAddPlayer(APlayerController* _PC)
{
	U4M_LOG_FUNC();
	if (_PC) {
		if (Players.Find(_PC) > 0)
			return;
		Players.AddUnique(_PC);
		cbNewPlayerController.Broadcast();
		if (Players.Num() == 1) {
			fSetWorldMaster(_PC);
		}
		//TEXT("TEXT("!GameStartEvent()") fSetEventState(+1);
	}
}

void UU4_GM_CAC::fRemovePlayer(APlayerController* _PC)
{
	if (_PC) {
		Players.Remove(_PC);
	}
}

APlayerController* 
UU4_GM_CAC::fGetPlayer(FString _player_name) 
{
	for ( APlayerController* p : Players) {
		check(p);
		if ( p->GetActorLabel( ) == _player_name ) {
			return p;
		}
	}
	return nullptr;
}

APlayerController* 
UU4_GM_CAC::fKickPlayer(FString _player_name, FText _reason) 
{
	if ( APlayerController* p = fGetPlayer(_player_name) ) {
		UU4_CoreLibrary::fLog("Player kick requested", p->GetActorLabel( ));
		AGameSession* game_session = GEngine->GetWorld()->GetAuthGameMode( )->GameSession;
		check(game_session);
		check(game_session->KickPlayer(p, _reason));
	}
	return nullptr;
}

void UU4_GM_CAC::fGameStart() {
	IsGameStart = 0x1;
	Events.fStartEvent(TEXT("!GameStartEvent()"));
}

UU4_Event_CAC* UU4_GM_CAC::fGetEventWithTag(FName _tag) const
{
	UWorld* world = GEngine->GetWorldFromContextObject(GetOwner(), EGetWorldErrorMode::LogAndReturnNull);
	check(world);

	for (TActorIterator<AActor> ait(world, AActor::StaticClass()); ait; ++ait) {
		AActor* actor = *ait;
		if (actor->ActorHasTag(_tag)) {
			// Event found, searching component.
			TArray<UActorComponent> all_events;
			for (UActorComponent* component : actor->GetComponents()) {
				if (component && component->ComponentHasTag(_tag)) {
					UU4_Event_CAC* result = Cast<UU4_Event_CAC>(component);
					return result;
				}
			}
		}
	}
	return nullptr;
}

bool
UU4_GM_CAC::fRegisterMap(UU4_Map_CAC* _map)
{
	if (CurrentMap != nullptr) {
		CurrentMap = _map;
		cbNewMap.Broadcast();
		return true;
	}
	else {
		return false;
	}
}

