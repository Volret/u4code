// Fill out your copyright notice in the Description page of Project Settings.

#include "..\Public\U4_GI_CPP.h"
#include "..\Lib\U4_CoreLibrary.h"
#include "Misc\MessageDialog.h"
#include "Internationalization/Internationalization.h"

#define LOCTEXT_NAMESPACE "U4_GI_CPP"

void UU4_GI_CPP::fControllerCheck(APlayerController* _controller, UU4_Controller_CAC* _cac)
{
	U4M_LOG_FUNC();
	InitializeChecker.Check();
}

void UU4_GI_CPP::fSelfTest(const UObject* _WorldContext)
{
	if (true) // Is need to show message.
		FMessageDialog::Open(EAppMsgType::Ok, LOCTEXT("U4_GI_TESTED", "U4 GameInstance test"));
}

void UU4_GI_CPP::fTravel(const UObject* _WorldContext, int _E, int _A)
{
	UWorld* World;
	FString Command;
	if (!LevelKeeper.fGetLevel(Command, _E, _A))
	{
		UU4_CoreLibrary::fRiseError("LevelKeeper", "Level Not Found!");
		return;
	}

	const ETravelType TravelType = TRAVEL_Absolute;

	World = GEngine->GetWorldFromContextObject(_WorldContext, EGetWorldErrorMode::LogAndReturnNull);
	if (World == nullptr) {
		UU4_CoreLibrary::fRiseError("U4 Game Instance", "World not found!");
		return;
	}

	FURL test_url(&WorldContext->LastURL, *Command, TravelType);
	if (!GEngine->MakeSureMapNameIsValid(test_url.Map)) {
		UE_LOG(LogLevel, Warning, TEXT("WARNING: The map '%s' does not exist."), *test_url.Map);
	}

	InitializeChecker.Reset();
	GEngine->SetClientTravel(World, *Command, TravelType);
}

void UU4_GI_CPP::fAddMap(FString _Map, int _Episode, int _Act)
{
	LevelKeeper.fAddLevel(_Episode, _Act, _Map);
}

void UU4_GI_CPP::fAddWorld(const TSoftObjectPtr<UWorld> _Level, int _Episode, int _Act)
{
	FString world_name = *FPackageName::ObjectPathToPackageName(_Level.ToString());
	fAddMap(world_name, _Episode, _Act);
}

#undef LOCTEXT_NAMESPACE