// Fill out your copyright notice in the Description page of Project Settings.


#include "U4_RTS_UnitSelection_CSC.h"

#include "Components/BoxComponent.h"
#include "UObject/ConstructorHelpers.h"

UU4_RTS_UnitSelection_CSC::UU4_RTS_UnitSelection_CSC()
{
	PrimaryComponentTick.bCanEverTick = false;
	
	fInitSelectionTree();
}


void UU4_RTS_UnitSelection_CSC::BeginPlay()
{
	Super::BeginPlay();

	// Try set up collision material.
	if(SelectionMaterial) {
		Selection.Collision->SetMaterial(0, SelectionMaterial);
	} else {
		ConstructorHelpers::FObjectFinder<UMaterialInstance> selection_mat =
			ConstructorHelpers::FObjectFinder<UMaterialInstance>(TEXT("Material'/Game/_Gameplay/Player/M0_ChaosSelection.M0_ChaosSelection'"));
		Selection.Collision->SetMaterial(0, selection_mat.Object);
	}
	// Try find controller.
	APawn* pawn = Cast<APawn>(GetOwner());
	if(pawn) {
		Selection.Controller = Cast<APlayerController>(pawn->GetController());
		check(Selection.Controller);
	}
}

void UU4_RTS_UnitSelection_CSC::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	fSelectionUpdate(DeltaTime);
}

void UU4_RTS_UnitSelection_CSC::fInitSelectionTree() 
{
	Selection.Collision = CreateDefaultSubobject<UStaticMeshComponent>("SelectionCollision");
	Selection.Root = CreateDefaultSubobject<USceneComponent>("SelectionRoot");


	Selection.Collision->SetFlags(RF_Transactional);

	ConstructorHelpers::FObjectFinder<UStaticMesh> selection_shape =
		ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));

	Selection.Collision->SetStaticMesh(selection_shape.Object);
	Selection.Collision->SetMobility(EComponentMobility::Movable);
	Selection.Collision->SetGenerateOverlapEvents(true);

//	Selection.Root->AttachTo(this);
	Selection.Root->SetRelativeTransform(FTransform(FQuat(), FVector(0, 0, 0), FVector(1, 1, 20)));

//	Selection.Collision->AttachTo(Selection.Root);
}

void UU4_RTS_UnitSelection_CSC::fSelectionBegin() {
	// Enable Selection physics.
	Selection.Collision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	// Show Selection cube.
	Selection.Collision->SetHiddenInGame(false);
	// Enable Selection ticks.
	PrimaryComponentTick.bCanEverTick = true;
}

void UU4_RTS_UnitSelection_CSC::fSelectionEnd() {
	// Disable Selection physics.
	Selection.Collision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	// Hide Selection cube.
	Selection.Collision->SetHiddenInGame(true);
	// Disable selection ticks.
	PrimaryComponentTick.bCanEverTick = false;
}

void UU4_RTS_UnitSelection_CSC::fSelectionDrop() {
	// Flush selection array.
	// SelectedObjects.RemoveAll();
}

void UU4_RTS_UnitSelection_CSC::fSelectionUpdate(float _time) {
	
	// Get mouse onscreen position.
	Selection.fUpdateSelection();
	Selection.fUpdateTransform(GetOwner(), _time);
}

void UU4_RTS_UnitSelection_CSC::Selection::fStartSelection() {
	fUpdateSelection();
	StartPosition = CurrentPosition;
}

void UU4_RTS_UnitSelection_CSC::Selection::fUpdateSelection() {
	Controller->GetMousePosition(CurrentPosition.X, CurrentPosition.Y);
}

void UU4_RTS_UnitSelection_CSC::Selection::fUpdateTransform(AActor* _actor, float _time) {
	static float time = 0;
	time += _time * 500;

	FTransform actor_transform = _actor->GetTransform();
	actor_transform.SetRotation(FQuat(1,0,0,time));
	Root->SetWorldTransform(actor_transform);
}
