// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "U4_RTS_UnitSelection_CSC.generated.h"

class AActor;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class U4_API UU4_RTS_UnitSelection_CSC : public USceneComponent
{
	GENERATED_BODY()


// Collizion Cube
public:
	struct Selection
	{
		UPROPERTY(EditAnywhere)
		class USceneComponent* Root;

		UPROPERTY(EditAnywhere)
		class UStaticMeshComponent* Collision;

		FVector2D StartPosition;
		FVector2D CurrentPosition;

		APlayerController* Controller;

		float CubeScale;
		FVector CubeWorldLocation;
		FRotator CubeWorldRotation;

	public:
		Selection() : Root(0x0), Collision(0x0), Controller(nullptr) {}
		// Get's mouse position to Start Position nd Current Position.
		void fStartSelection();
		// Update's Current Mouse Position.
		void fUpdateSelection();
		// Update collision world transform.
		void fUpdateTransform(AActor* _actor, float _time);

	}Selection;

	TArray<AActor> SelectedObjects;

	// Start selecting actors. Show Selection cube, enable selection phyzics, add to array all selected actors.
	void fSelectionBegin();
	// Finish de selection. Hide cube, disable physics.
	void fSelectionEnd();
	// Flush selection.
	void fSelectionDrop();
	// Update seletion position and scale.
	void fSelectionUpdate(float _time);

public:
	UPROPERTY(EditAnywhere)
		UMaterialInterface* SelectionMaterial;

	UPROPERTY(EditAnywhere)
		UStaticMesh* SelectionShape;

public:	
	// Sets default values for this component's properties
	UU4_RTS_UnitSelection_CSC();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void fInitSelectionTree();
	void fUpdateSelectionTransform();

		
};
