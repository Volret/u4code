// Fill out your copyright notice in the Description page of Project Settings.

#include "../Map/U4_Map_CAC.h"
#include "GameFramework/GameState.h"

#include "../Public/U4_GS_CAC.h"

// Sets default values for this component's properties
UU4_Map_CAC::UU4_Map_CAC()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UU4_Map_CAC::BeginPlay()
{
	Super::BeginPlay();

	// Try Register in Game Mode.
	AGameModeBase* game_mode = GetWorld()->GetAuthGameMode();
	if (game_mode) {
		UU4_GM_CAC* gm_cac = game_mode->FindComponentByClass<UU4_GM_CAC>();
		if (gm_cac)
			gm_cac->fRegisterMap(this);
	}
	
	// Try Register in Game State.
	AGameStateBase* game_state = GetWorld()->GetGameState();
	if (game_state) {
		UU4_GS_CAC* gs_cac = game_state->FindComponentByClass<UU4_GS_CAC>();
		if (gs_cac) {
			gs_cac->fRegisterMap(this);
		}
	}
}

// Called every frame
void UU4_Map_CAC::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

